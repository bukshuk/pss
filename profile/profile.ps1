﻿#
# profile.ps1
#

Import-Module posh-git

Import-Module -Force -DisableNameChecking Git
Import-Module -Force -DisableNameChecking Profile
Import-Module -Force -DisableNameChecking Project
Import-Module -Force -DisableNameChecking Elcom

Set-GitAlias
Set-ProfileAlias
Set-ProjectAlias
Set-ElcomAlias

$current = Get-Location
$projects = Get-ProjectsPath
if ($current -notlike "${projects}*") {
	Set-Location $projects
}
