#
# Docker.psm1
#

function Set-DockerAlias() {
	Set-Alias -Scope Global dcon Get-DockerContainers
	Set-Alias -Scope Global dimg Get-DockerImages
	Set-Alias -Scope Global drec Remove-ExitedDockerContainers
	Set-Alias -Scope Global drdi Remove-DanglingDockerImages
	Set-Alias -Scope Global drdv Remove-DanglingDockerVolumes
	Set-Alias -Scope Global dclr Clear-DockerHost
}

function Get-DockerContainers($options = "") {
	Clear-Host
	Write-Host

	$format = "ID\t{{.ID}}\nIMAGE\t{{.Image}}\nCOMMAND\t{{.Command}}\nCREATED\t{{.RunningFor}}\nSTATUS\t{{.Status}}\nPORTS\t{{.Ports}}\nNAMES\t{{.Names}}\n"
	docker container ls -a --format $format $options
}


function Get-DockerImages($options = "") {
	Clear-Host
	Write-Host

	$format = "Repository:\t{{.Repository}}\nTag:\t\t{{.Tag}}\nID:\t\t{{.ID}}\nCreated:\t{{.CreatedSince}}\nSize:\t\t{{.Size}}\n"
	docker images --format $format $options
}

function Remove-ExitedDockerContainers() {
	docker container rm $(docker container ls -aqf "status=exited")
}

function Remove-DanglingDockerImages() {
	docker image rm $(docker image ls -qf "dangling=true")
}

function Remove-DanglingDockerVolumes() {
	docker volume rm $(docker volume ls -qf "dangling=true")
}

function Clear-DockerHost() {
	docker system prune -a
}
