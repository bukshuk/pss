#
# Git.psm1
#

function Set-GitAlias() {
	Set-Alias -Scope Global gitcl Git-Clone
	Set-Alias -Scope Global gitlg Git-Log 
	Set-Alias -Scope Global gitst Git-Status
	Set-Alias -Scope Global gitcm Git-Commit
	Set-Alias -Scope Global gitx Git-RunExtensionsBrowse
	Set-Alias -Scope Global gitxc Git-RunExtensionsCommit
}

function Git-Clone($repo)
{
	if ([io.path]::GetDirectoryName($repo) -eq "")
	{
		$values = Find-Project($repo);
		$repoName = $values[0]
		$repoUrl = $values[1]
	}
	else
	{
		$repoUrl = $repo
		$repoName = [io.path]::GetFileNameWithoutExtension($repo)
	}

	if ($repoName -eq $null)
	{
		Write-Host -ForegroundColor Red "Cannot find ${repo}"
		return
	}

	if (Test-Path $repoName)
	{
		Write-Host -ForegroundColor Red "${repoName} exists"
		return
	}
	
	git clone --recurse-submodules "${repoUrl}"
	Set-Location "${repoName}"
	Git-RemoveRemoteHead
	Write-Host ""

	Git-LogCommand 5
	Write-Host ""
}

function Git-Status {
	Clear-Host
	git status -u -s
	Write-Host ""
}

function Git-Log {
	Clear-Host
	Write-Host ""
	Git-LogCommand 25
	Write-Host ""
}

function Git-LogCommand($depth) {
	git log --oneline --graph --decorate -$depth
}

function Git-Commit($message) {
	Write-Host ""
	git commit -am "${message}"
	Write-Host ""
}

function Git-RunExtensionsBrowse {
	gitex
}

function Git-RunExtensionsCommit {
	gitex commit
}

function Git-RemoveRemoteHead($remoteName = "origin") {
	git remote set-head "${remoteName}" -d
}
