#
# Elcom.psm1
#

function Set-ElcomAlias() {
	Set-Alias -Scope Global rsgt Get-SpttService
	Set-Alias -Scope Global rsst Start-SpttService
	Set-Alias -Scope Global rssp Stop-SpttService
	Set-Alias -Scope Global rsrs Restart-SpttService
}

$spttServiceName = "SmartPTT Radio Service"

function Get-SpttService()
{
	Get-WmiObject win32_service | ?{$_.Name -like 'smartptt*'} | select State, PathName, Name
}

function Start-SpttService()
{
	Start-Service -Name $spttServiceName
	Get-SpttService
}

function Stop-SpttService()
{
	Stop-Service -Name $spttServiceName
	Get-SpttService
}

function Restart-SpttService()
{
	Restart-Service -Name $spttServiceName
	Get-SpttService
}
