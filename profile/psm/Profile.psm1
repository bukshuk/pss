#
# Profile.psm1
#

function Set-ProfileAlias() {
	Set-Alias -Scope Global touch Touch-File
	Set-Alias -Scope Global spc Stop-Computer
	Set-Alias -Scope Global rpc Restart-Computer
}

function Sleep-Computer {
	$minutes = Read-Host "Enter minutes to wait until sleep";
	$seconds = 60 * $minutes;
	Start-Sleep -Seconds $seconds

	Add-Type -Assembly System.Windows.Forms
	[System.Windows.Forms.Application]::SetSuspendState("Suspend", $false, $true)
}


function Get-EnvironmentVariable($name) {
	$envar = [environment]::GetEnvironmentVariable($name, "Process")

	if (!$envar) {
		Write-Host -ForegroundColor Red "${name} is not set"
		return
	}

	return $envar
}

function Find-Project($searchString)
{	
	$myDocuments = [Environment]::GetFolderPath("MyDocuments")
	$csvFile = Import-Csv "${myDocuments}\myrepos.csv"
	
	foreach ($entry in $csvFile)
	{
		if ($entry.project -match $searchString)
		{
			$project = $entry.project
			$repository = $entry.repository  
			$solution = $entry.solution
		}
	}

	return $project, $repository, $solution
}

function Touch-File($path) {
	if (Test-Path $path) {
		$date = Get-Date
		$item = Get-Item -Path $path
		# Update timestamps
		$item.CreationTime = $date
		$item.LastAccessTime = $date
		$item.LastWriteTime = $date
	}
	else {
		# Create directories recursively
		$dir = Split-Path -Path "${path}"
		if ($dir -and (!(Test-Path "${dir}"))) {
			New-Item -ItemType Directory -Path "${dir}"
		}
		# Create file
		New-Item -ItemType File -Path "${path}"
	}
}

