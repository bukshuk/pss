#
# Project.psm1
#

function Set-ProjectAlias() {
	Set-Alias -Scope Global pr Run-Project
	Set-Alias -Scope Global prt Run-ProjectTestDiff
}

function Get-ProjectsPath
{
	$projectsPath = Get-EnvironmentVariable PROJECTS

	return $projectsPath
}

function Run-Project($projectNameSeachString = "") 
{
	if ($projectNameSeachString -eq "")
	{
		$projectPath = Get-Location
		$projectNameSeachString = [io.path]::GetFileName($projectPath)
	}

	$values = Find-Project($projectNameSeachString)
	$projectName = $values[0]
	$solution = $values[2]

	if ([string]::IsNullOrWhiteSpace($projectName))
	{
		$projectName = $projectNameSeachString
	}
	
	if ([string]::IsNullOrWhiteSpace($solution)) 
	{
		$solution = "${projectName}.sln"
	}
	
	$projectPath = "$(Get-ProjectsPath)\${projectName}"
	
	if (!(Test-Path $projectPath))
	{
		Write-Host -ForegroundColor Red "${projectName} project is not found"
		return
	}

	Set-Location $projectPath

	if (Test-Path $solution)
	{
		Invoke-Item $solution
	}
	else
	{
		Code.cmd $projectPath
	}
}

function Run-ProjectTestDiff($projectName = "")
{
	$projectPath = Get-Location

	if ($projectName -eq "")
	{
	  $projectName = [io.path]::GetFileName($projectPath)
	}
	
	$testProjectPath = "${projectPath}\${projectName}.test"
	if (!(Test-Path $testProjectPath))
	{
		Write-Host -ForegroundColor Yellow "The project has no tests"
		Write-Host
		return
	}
	
	$checkTestPath = "${testProjectPath}\check"
	if (!(Test-Path $checkTestPath)) 
	{
		Write-Host -ForegroundColor Red "Unknown test project structure"
		Write-Host
		return
	}

	$outputTestPath = "${testProjectPath}\bin\test"
	if (!(Test-Path $outputTestPath))
	{
		Write-Host -ForegroundColor Green "All tests seem passed"
		Write-Host
		return
	}

	Run-DiffTool $checkTestPath $outputTestPath
}

function Run-DiffTool($firstEntry, $secondEntry) {
	Compare.exe /max $firstEntry $secondEntry
}

function Run-BashHere($script = "") {
	$args = @()
	if ($script -ne "") {
		$args = @("-c", $script)
	}

	git-bash.exe $args
}

function Npm-Start {
	npm start --silent
}

function Npm-Share {
	npm run share --silent
}

function Npm-Test {
	npm test --silent
}
