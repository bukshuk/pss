﻿#
# Backup.psm1
#

function Backup-Computer {
	$documents = [Environment]::GetFolderPath("MyDocuments")
	$pictures = [Environment]::GetFolderPath("MyPictures")
	$desktop = [Environment]::GetFolderPath("Desktop")
	$system = [Environment]::GetFolderPath("System")

	$appdata = Get-EnvironmentVariable("APPDATA")
	$profile = Get-EnvironmentVariable("USERPROFILE")

	$targets = @(
		"d:\docs",
		"${profile}\.git*",
		"${profile}\.bash_profile",
		"${appdata}\Microsoft\Templates\*.oft",
		"${appdata}\Microsoft\Excel\XLSTART",
		"${appdata}\Microsoft\Sticky Notes",
		"${documents}",
		"${desktop}",
		"${pictures}",
		"${system}\drivers\etc\hosts"
	)

	$archiveFolderPath = Get-ArchiveFolderPath
	$archivePrepass = Get-ArchivePrepass

	if (($archiveFolderPath -ne "") -and ($archivePrepass -ne "")) {
		$archive = "${archiveFolderPath}{0}-{1:00}-{2:00}.7z" -f (Get-Date).Year, (Get-Date).Month, (Get-Date).Day
		if (Test-Path $archive) {
			Remove-Item $archive
		}

		foreach ($t in $targets) {
			Archive-Target $archive $t $archivePrepass
		}
	}
}

function Get-ArchivePrepass {
	$path = Get-ArchiveFolderPath

	if ($path -eq "") {
		return
	}

	$prepassfile = "${path}info.txt"

	if (Test-Path $prepassfile) {
		return Get-Content $prepassfile
	}

	return ""
}

function Get-ArchiveFolderPath {
	$dir = "archive"
	$drives = Get-PSDrive | Where {$_.Provider.Name -eq "FileSystem"} | Foreach-Object {$_.Root}

	foreach ($d in $drives) {
		if (Test-Path "${d}${dir}") {
			return "${d}${dir}\"
		}
	}

	return ""
}

function Archive-Target($archive, $target, $passfix) {
	7z.exe a -mx1 $archive $target -p"dmJ${passfix}" -mhe
}

function Run-SourceTree {
	C:\Users\bukshuk\AppData\Local\SourceTree\Update.exe --processStart "SourceTree.exe"
}