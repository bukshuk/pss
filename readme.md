## Install

* clone the repo by the command `git clone --recurse-submodules https://gitlab.com/bukshuk/pss.git`
* set `PROJECTS` env to a working directory
* run `deploy_pss.bat` from the repo root
* the repo can be removed after

## Commands

### Profile
| Command          | Alias |
|------------------|-------|
| Stop-Computer    | spc   |
| Restart-Computer | rpc   |
| Touch-File       | touch |

### Git
| Command                 | Alias |
|-------------------------|-------|
| Git-Clone               | gitcl |
| Git-Log                 | gitlg |
| Git-Status              | gitst |
| Git-Commit              | gitcm |
| Git-RunExtensionsBrowse | gitx  |
| Git-RunExtensionsCommit | gitxc |

### Elcom
| Command                       | Alias |
|-------------------------------|-------|
| Get-SpttService               | rsgt  |
| Start-SpttService             | rsst  |
| Stop-SpttService              | rssp  |
| Restart-SpttService           | rsrs  |
