@ECHO OFF

IF "%USERPROFILE%" == "" GOTO NOUSERPROFILE

SET TARGET=%USERPROFILE%\Documents\WindowsPowerShell\

XCOPY /Y "profile\profile.ps1" "%TARGET%"

REM Use COPY as it creates file without the file or directory prompt
COPY /Y "profile\PowerShell_profile.ps1" "%TARGET%Microsoft.PowerShell_profile.ps1"
COPY /Y "profile\PowerShell_profile.ps1" "%TARGET%Microsoft.PowerShellISE_profile.ps1"

XCOPY /Y "profile\psm\Git.psm1" "%TARGET%\Modules\Git\"
XCOPY /Y "profile\psm\Profile.psm1" "%TARGET%\Modules\Profile\"
XCOPY /Y "profile\psm\Project.psm1" "%TARGET%\Modules\Project\"
XCOPY /Y "profile\psm\Elcom.psm1" "%TARGET%\Modules\Elcom\"

XCOPY /Y "posh-git\src\*" "%TARGET%\Modules\posh-git\"

ECHO.
ECHO.
ECHO   Run .$profile.CurrentUserAllHosts from a running PowerShell client !!!
ECHO.

TIMEOUT /T 10

GOTO END

:NOUSERPROFILE
ECHO The USERPROFILE environment variable is not set

:END